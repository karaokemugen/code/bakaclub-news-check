
import fs from 'fs';
import axios from 'axios';
import {basename} from 'path';
import Downloader from 'nodejs-file-downloader';

const configfile = fs.readFileSync('config.json');
const config = JSON.parse(configfile);

axios.get('https://kurisu.iiens.net/api?force_dl', {
        headers: {
                'X-Token-Authorization': config.header
        }
}).then((res) => {
        //console.log(JSON.stringify(res.data, null, 2));
        parseBakaClubKaras(res.data);
}).catch((err) => {
        console.log(err);
});

async function parseBakaClubKaras(karas) {
        const newKaras = karas.filter(k => k.author_year >= (process.argv[2] + ' 00:00:00'));
        console.log(newKaras);
        for (const kara of newKaras) {
                try {
                        await gitlabPostNewIssue(kara);
                } catch(err) {
                        // Do nothing.
                }
        }
}

async function gitlabPostNewIssue(kara) {
        const karaname = `${kara.language.toUpperCase()} - ${kara.source_name} - ${kara.song_type}${kara.song_number || ''} - ${kara.song_name}`;
        console.log(karaname);
        const downloader = new Downloader({
         url: `https://kurisu.iiens.net/api/download/${kara.id}`,
         headers: {
                         'X-Token-Authorization': config.header
         },
         directory: './downloads',
         cloneFiles: false
        });
        let filepath = '';
        try {
                const res = await downloader.download();
                filepath = res.filePath;
        } catch(err) {
                console.log(err);
                throw err;
        }
        console.log(filepath);
        const params = {
                id: '32123952',
                title: `[Import Bakaclub] ${karaname}`,
                description: `
Download URL : https://kara.moe/bakaclub/${encodeURIComponent(basename(filepath))}

Auteur : ${kara.author_name}

Catégorie : ${kara.category}

Commentaire :
` + (kara.upload_comment ? `${kara.upload_comment}` : ''),
                labels: 'To Add'
        };
        const res = await axios.post(`https://gitlab.com/api/v4/projects/32123952/issues?${params.toString()}`, params, {
                        headers: {
                                'PRIVATE-TOKEN': config.gitlabToken
                        }
        });
}
