# Bakaclub News Check

Small script to get new songs from the Bakaclub kara repository and download them.

You'll need an auth token for the Bakaclub server as well as a token for posting gitlab issues to the karaoke mugen base.

Code could need some cleanup/modernization as it was written a while ago.